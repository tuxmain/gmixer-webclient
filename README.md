# ĞMixer web client

This is a JavaScript in-browser client for [the ĞMixer protocol](https://git.duniter.org/tuxmain/gmixer-py).

**Warning**: This is **not** about NodeJS. I spent enough time transforming NodeJS libs into correct JS.

[There is an instance online, try it!](http://zettascript.org/tux/g1/gmixer-webclient)

Currently, it can:

 * List ĞMixer nodes in real-time;
 * Build a mixing path;
 * Generate a mix request, but the crypto functions are not yet implemented.

The browser (e.g. Firefox) can refuse to send HTTP requests from a page loaded via HTTPS. Thus, ĞMixer web client must be available with HTTP. (this is why I do not like modern browsers nor permanent 301 redirections to HTTPS)
