/*
	Copyright 2019 Pascal Engélibert
	This file is part of ĞMixer web client.

	ĞMixer web Client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ĞMixer web client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with ĞMixer web client.  If not, see <https://www.gnu.org/licenses/>.
*/

var path = null;

function bool2html(a) {
	if(a)
		return '<span class="bool true">YES</span>';
	else
		return '<span class="bool false">NO</span>';
}

function update_nodes_display() {
	var tbody = $("#nodes-body");
	var peersfile = $("#nodes-peers_file");
	tbody.empty();
	peersfile.empty();
	for(node in nodes) {
		tbody.append(nodes[node].export_html_tr());
		if(nodes[node].up)
			peersfile.append(nodes[node].pubkey+" "+nodes[node].host+" "+nodes[node].port+"\r\n");
	}
}

function update_nodes(data) {
	$("#nodes-log").html("List provider: "+data["pubkey"]);
	if(data["pubkey"] in nodes_index)
			nodes_index[data["pubkey"]].up = true;
	
	for(node in data["peers"]) {
		if(data["peers"][node]["pubkey"] in nodes_index)
			nodes_index[data["peers"][node]["pubkey"]].up = data["peers"][node]["up"];
		else {
			var new_node = new Node(data["peers"][node]["host"], data["peers"][node]["port"], data["peers"][node]["pubkey"], data["peers"][node]["up"]);
			nodes.push(new_node);
			nodes_index[data["peers"][node]["pubkey"]] = new_node;
		}
	}
	try_nodes = nodes.slice();
	update_nodes_display();
	setTimeout(query_node, 30000);
}

function query_node() {
	if(try_nodes.length == 0) {
		$("#nodes-log").html("Error: no node reachable.");
		try_nodes = nodes.slice();
		setTimeout(query_node, 10000);
		return;
	}
	var node = try_nodes.splice(Math.floor(Math.random()*try_nodes.length), 1)[0];
	console.log("Query "+node.host+":"+node.port);
	$.ajax({
		url: "http://"+node.host+":"+node.port+"/json/list/pubkey",
		success: update_nodes,
		error: function(jqXHR, textStatus, errorThrown) {
			if(node.pubkey in nodes_index)
				nodes_index[node.pubkey].up = false;
			
			query_node();
		},
		timeout: 5000
	});
}

async function mix_display_pubkey() {
	var id = $("#mix-sender-id").val();
	var salt = $("#mix-sender-salt").val();
	if(id == "" || salt == "") {
		$("#mix-sender-pubkey").value = "";
		return;
	}
	var keys = await idSecPass2cleanKeys(id, salt);
	console.log(keys);
	$("#mix-sender-pubkey").val(keys.publicKey);
}

function build_display_path() {
	path = build_path();
	
	var html = '<table border="1">';
	for(node in path) {
		html += path[node].export_html_tr(false);
	}
	$("#mix-path").html(html);
}

async function ui_mix() {
	if(!path)
		return;
	
	var amount = parseInt($("#mix-amount").val());
	var id = $("#mix-sender-id").val();
	var salt = $("#mix-sender-salt").val();
	if(id == "" || salt == "")
		return;
	var sender = await idSecPass2cleanKeys(id, salt);
	var receiver = $("#mix-receiver-pubkey").val();
	
	await mix(path, amount, sender, receiver);
}

window.onload = function() {
	try_nodes = nodes.slice();
	query_node();
};
