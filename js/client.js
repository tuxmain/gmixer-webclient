/*
	Copyright 2019 Pascal Engélibert
	This file is part of ĞMixer web client.

	ĞMixer web Client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ĞMixer web client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with ĞMixer web client.  If not, see <https://www.gnu.org/licenses/>.
*/

// ---- Classes

class Node {
	constructor(host, port, pubkey="", up=null) {
		this.pubkey = pubkey;
		this.host = host;
		this.port = port;
		this.up = up;
	}
	
	export_html_tr(show_up=true) {
		if(show_up)
			return "<tr><td>"+this.pubkey+"</td><td>"+this.host+":"+this.port+"</td><td>"+bool2html(nodes[node].up)+"</td></tr>";
		else
			return "<tr><td>"+this.pubkey+"</td><td>"+this.host+":"+this.port+"</td></tr>";
	}
}

// ---- INIT

const ubjson_encoder = new UbjsonEncoder({optimizeArrays:true});

var nodes = [
	new Node("svetsae7j3usrycn.onion", 10951, "4QRZj4bHpXTdJajfX8mTf2RmYQhKX7BtiMiZL3z5Lo8F"),
	new Node("svetsae7j3usrycn.onion", 10952, "FE4p3w7LvfDtat7pwNky5vZ9SPGKRrFid8gS2bg8Pjka"),
	new Node("185.193.38.231", 10951, "HYm6mvGz4JyKdUWwYsJsdTVCfMx3mGaZgQeNtdgSGPFN"),
	new Node("37.59.36.94", 10951, "wwtwMNBLJt7AQ5mWQeyGKddgjLtpRdyTTPYuMBSAKcG"),
	new Node("51.68.124.31", 10951, "HsYcZNRjjXEv98vpCbNdQuAHaTCXV6fCNu8tKC1xkGjR")
];
var nodes_index = {};
var try_nodes = [];

for(node in nodes) {
	nodes_index[nodes[node].pubkey] = nodes[node];
}

// ---- FUNCTIONS

function concat(a, b) {
	var c = new (a.constructor)(a.length + b.length);
	c.set(a, 0);
	c.set(b, a.length);
	return c;
}

function url_token(l=22) {
	var token = "";
	while(token.length < l) {
		token += Math.random().toString(36).substr(2);
	}
	return token.substr(token.length - l);
}

function build_path(layers=3) {
	if(nodes.length < layers) {
		console.log("Error: not enough nodes!");
		return false;
	}
	var i_nodes = nodes.slice();
	var path = [];
	while(path.length < layers && i_nodes.length > 0) {
		var node = i_nodes.splice(Math.floor(Math.random()*i_nodes.length), 1)[0];
		if(node.up)
			path.push(node);
	}
	if(path.length < layers) {
		console.log("Error: not enough up nodes!");
		return false;
	}
	return path;
}

function query_node() {
	if(try_nodes.length == 0) {
		try_nodes = nodes.slice();
		setTimeout(query_node, 10000);
		return false;
	}
	var node = try_nodes.splice(Math.floor(Math.random()*try_nodes.length), 1)[0];
	console.log("Query "+node.host+":"+node.port);
	$.ajax({
		url: "http://"+node.host+":"+node.port+"/json/list/pubkey",
		success: update_nodes,
		error: function(jqXHR, textStatus, errorThrown) {
			if(node.pubkey in nodes_index)
				nodes_index[node.pubkey].up = false;
			
			query_node();
		},
		timeout: 5000
	});
	return true;
}

async function mix(path, amount, sender, receiver) {
	var path = path.slice();
	var onetime_keys = [];
	var comment_seeds = [[
		crypto.getRandomValues(new Uint8Array(32)),
		null,
		crypto.getRandomValues(new Uint8Array(32))
	]];
	message = new Uint8Array(0);
	
	i = path.length - 1
	path.push({receiver: receiver})
	while(i >= 0) {
		var ot_id = url_token();
		var ot_salt = url_token();
		var ot_keys = await idSecPass2cleanKeys(ot_id, ot_salt);
		onetime_keys.unshift(ot_keys);
		var out_seeds = comment_seeds[0];
		var in_seeds = [
			crypto.getRandomValues(new Uint8Array(32)),
			null,
			null
		];
		comment_seeds.unshift(in_seeds);
		message = ubjson_encoder.encode({
			"receiver": path[i+1].receiver,
			"onetime": ot_keys.publicKey,
			"in_seeds": in_seeds,
			"out_seeds": out_seeds,
			"message": message
		});
		i --;
	}
	console.log(message);
}
